//
//  ViewController.swift
//  DifferentialLine
//
//  Created by Brian Broom on 11/14/16.
//  Copyright © 2016 Brian Broom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    let points = LinkedList()
    
    let centerPoint = view.center
    let radius = CGFloat(100.0)
    
    for i in 0...10 {
      let deltaRadians = CGFloat(2.0) * CGFloat(M_PI) / 10
      let angle = CGFloat(i) * deltaRadians
      let dX = radius * cos(angle)
      let dY = radius * sin(angle)
      let newPoint = CGPoint(x: centerPoint.x + dX, y: centerPoint.y + dY)
      points.append(value: newPoint)
    }
    
    let v = view as! LineConnectedPointView
    v.points = points
    
    print(points)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

