//
//  Node.swift
//  DifferentialLine
//
//  Created by Brian Broom on 11/14/16.
//  Copyright © 2016 Brian Broom. All rights reserved.
//

import CoreGraphics

class Node {
  var point :CGPoint
  var next :Node?
  weak var prev :Node?
  
  init(point :CGPoint) {
    self.point = point
  }
}

class LinkedList {
  var head :Node?
  var tail :Node?
  
  var isEmpty :Bool {
    return head == nil
  }
  
  var first :Node? {
    return head
  }
  
  var last :Node? {
    return tail
  }
  
  func insert(node :Node, after oldNode :Node) {
    
  }
  
  func append(value :CGPoint) {
    let newNode = Node(point: value)
    
    if let tailNode = tail {
      newNode.prev = tailNode
      tailNode.next = newNode
    } else {
      head = newNode
    }
    
    tail = newNode
  }
}

extension LinkedList: CustomStringConvertible {
  public var description: String {
    var text = "["
    var node = head
    
    while node != nil {
      text += "(\(node!.point.x),\(node!.point.y))"
      node = node!.next
      if node != nil {  text += ", " }
    }
    return text + "]"
  }
}






