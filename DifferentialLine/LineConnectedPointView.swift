//
//  LineConnectedPointView.swift
//  DifferentialLine
//
//  Created by Brian Broom on 11/18/16.
//  Copyright © 2016 Brian Broom. All rights reserved.
//

import UIKit

class LineConnectedPointView: UIView {
  
  var points: LinkedList?
  
  // Only override draw() if you perform custom drawing.
  // An empty implementation adversely affects performance during animation.
  override func draw(_ rect: CGRect) {
    if let points = points {
      if points.isEmpty { return }
      
      UIColor.green.setFill()
      let outline = UIBezierPath()
      
      var node = points.head
      let startPoint = node!.point
      outline.move(to: startPoint)

      while node != nil {
        let p = node!.point
        let pointSize: CGFloat = 20
        let pointRect = CGRect(x: p.x - pointSize/2, y: p.y - pointSize/2, width: pointSize, height: pointSize)
        let path = UIBezierPath(ovalIn: pointRect)
        path.fill()
        
        node = node!.next
        if node != nil {
          outline.addLine(to: node!.point)
        } else {
          outline.close()
        }
      }
      
      outline.stroke()
    }
  }
  

}
